onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/clk
add wave -noupdate /conv_ctrl_test/rst
add wave -noupdate -radix unsigned -radixshowbase 0 /conv_ctrl_test/cnvs_cntrl_cnt
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/SYNC
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/cnv_state
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/CONVST_A
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/BUSY
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/SPI_READY
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/CS
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/fsm_spi_curr_state
add wave -noupdate /conv_ctrl_test/circ_conv_cntrl/sclk
add wave -noupdate -radix unsigned -radixshowbase 0 /conv_ctrl_test/circ_conv_cntrl/cnvs_cntrl_cnt
add wave -noupdate -radix unsigned -radixshowbase 0 /conv_ctrl_test/circ_conv_cntrl/spi_transmit_cnt
add wave -noupdate -radix unsigned -radixshowbase 0 /conv_ctrl_test/circ_conv_cntrl/sclk_devider_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1177 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 132
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {26608 ns}
