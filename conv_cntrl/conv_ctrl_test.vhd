library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity conv_ctrl_test is
    generic(
        CNVS_CLK_LP : integer :=  4;   -- std_logic_vector(4 downto 0) "00010"; --integer --:=  5; 

        S_pulse     : integer :=  6
    );
end conv_ctrl_test;

architecture conv_ctrl_test_sruct of conv_ctrl_test is

component conv_cntrl
    port (
        clk  : in std_logic;
        rst  : in std_logic;

        SYNC : in std_logic;
        BUSY_b : in std_logic;
        SDATA: in std_logic;

        CS       : out std_logic;
        RESET    : out std_logic;
        CONVST_A : out std_logic;
        CONVST_B : out std_logic;
        ODATA    : out std_logic_vector(17 downto 0)
    );
end component;

--! type sync_states is ( waitSYNC, CNV_LP, waitBUSY);
--! signal sync_state, sync_next_state : sync_states;

signal clk  : std_logic := '0';
signal rst  : std_logic := '0';

signal cnvs_cntrl_cnt : std_logic_vector(6 downto 0) := (others => '0');
signal BUSY_cntrl_cnt : std_logic_vector(6 downto 0) := (others => '0');
--! signal BUSY : std_logic := '0';
signal SYNC : std_logic := '0';
signal CONVST_A : std_logic;
signal CONVST_B : std_logic;
signal ODATA    : std_logic_vector(17 downto 0);

begin

process
 begin
    wait for 20 ns;
    clk <= not clk;
end process;

-- process
--  begin
--     wait for 40 ns;
--     sclk <= not sclk;
-- end process;



-- process(clk) 
--  begin
--     if rising_edge(clk) then 
--         sync_state <= sync_next_state;
--     end if ;
-- end process;

-- process(clk, SYNC, BUSY_cntrl_cnt)
--  begin

--     case( sync_state ) is
    
--         when waitSYNC =>
--             if SYNC = '1' then
--                 sync_next_state <= CNV_LP;                
--             end if ;
        
--         when CNV_LP =>
--             if (unsigned(BUSY_cntrl_cnt) < CNVS_CLK_LP -1 ) then
--                 sync_next_state <= CNV_LP;
--                 if rising_edge(clk) then
--                     BUSY_cntrl_cnt <= std_logic_vector(unsigned(BUSY_cntrl_cnt) + 1);
--                 end if;
--             else
--                 sync_next_state <= waitBUSY;
--             end if;


--         when waitBUSY =>
--             if (unsigned(BUSY_cntrl_cnt) < 30) then
--                 sync_next_state <= waitBUSY;
--                 if rising_edge(clk) then
--                     BUSY_cntrl_cnt <= std_logic_vector(unsigned(BUSY_cntrl_cnt) + 1);
--                 end if;
--             else
--                 sync_next_state <= waitSYNC;
--                 if rising_edge(clk) then
--                     BUSY_cntrl_cnt <= (others => '0');
--                 end if;
--             end if;
--     end case ;

-- end process;

process(clk)
 begin
    if rising_edge(clk) then 
        if unsigned(cnvs_cntrl_cnt) = 80 then
            cnvs_cntrl_cnt <= (others => '0');
        else 
            cnvs_cntrl_cnt <= std_logic_vector(unsigned(cnvs_cntrl_cnt)+1) ;                
        end if ;
    end if;
end process;

SYNC <= '1' when unsigned(cnvs_cntrl_cnt)  = 10  else '0';  --!or unsigned(cnvs_cntrl_cnt) = 20 or unsigned(cnvs_cntrl_cnt) = 50 
--!BUSY <= '1' when sync_state = waitBUSY else '0';




rst <= '1' after 6300 ns,
       '0' after 6340 ns;
    --    '1' after 2180 ns,
    --    '0' after 2200 ns;
    --    '1' after 1180 ns,
    --    '0' after 1300 ns;

circ_conv_cntrl: conv_cntrl
                    port map(
                        clk => clk,
                        --sclk => sclk,
                        rst => rst,
                        BUSY_b => '0',
                        SYNC => SYNC,
                        SDATA => '1',
                        CONVST_A => CONVST_A,
                        CONVST_B => CONVST_b,
                        ODATA => open
                    );

end conv_ctrl_test_sruct ; -- conv_ctrl_test_sruct