library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity conv_cntrl is
    generic(
        --CNVS_CLK_HP       : integer := 32;   -- std_logic_vector(4 downto 0) "01101"; --integer --:= 15; 
        CNVS_CLK_LP       : integer :=  4;   -- std_logic_vector(4 downto 0) "00010"; --integer --:=  5;   
        CLK_DIV           : integer :=  2;
        SPI_TRANMIT_DELAY : integer :=  18



    );
    port (
        clk  : in std_logic;
        rst  : in std_logic;

        SYNC : in std_logic;
        BUSY_b : in std_logic;
        SDATA: in std_logic;

        CS       : out std_logic;
        RESET    : out std_logic;
        CONVST_A : out std_logic;
        CONVST_B : out std_logic;
        ODATA    : out std_logic_vector(17 downto 0) 
    );
end conv_cntrl;

architecture conv_cntrl_behavioral of conv_cntrl is

    type sync_states is ( waitSYNC, CNV_LP, waitBUSY);
    signal sync_state, sync_next_state : sync_states;


    type cnv_states is (cnv_idle, conv);
    signal cnv_state, cnv_next_state : cnv_states;


    type spi_state is (spi_reset, idle, transmit);
    signal fsm_spi_curr_state, fsm_spi_next_state : spi_state;


    signal BUSY_cntrl_cnt : std_logic_vector(6 downto 0) := (others => '0');

    signal cnvs_cntrl_cnt : std_logic_vector(4 downto 0) := (others => '0'); --(others => '0');

    signal spi_transmit_cnt : std_logic_vector(7 downto 0) := (others => '0'); --(others => '0');

    signal sclk_devider_cnt : std_logic_vector(4 downto 0) := "00010"; --(others => '0');          --!manipulating with the init value influence t25 parameter | init with CLK_DIV -> the lowest  delay(Tclk)
                                                                                                   --!reset counter must be accomodate with the init value     | init with 0       -> the highest delay(CLK_DIV * Tclk)

    signal sclk      : std_logic := '1';


    signal BUSY : std_logic;
    signal CONVST_X : std_logic;

    signal SPI_READY : std_logic := '1';

    signal SRGn : std_logic_vector(17 downto 0) := (others => '0');
begin


--! ADC BUSY signal implementation
process(clk) 
 begin
    if rising_edge(clk) then 
        sync_state <= sync_next_state;
    end if ;
end process;

process(clk, CONVST_X, BUSY_cntrl_cnt)
 begin
    if rising_edge(clk) then
        if (rst = '1') then 
            BUSY_cntrl_cnt <= (others => '0');
        end if;
    end if;

    case( sync_state ) is
    
        when waitSYNC =>
            if CONVST_X = '0' then
                sync_next_state <= CNV_LP;                
            end if ;
        
        when CNV_LP =>
            if (unsigned(BUSY_cntrl_cnt) < CNVS_CLK_LP -2 ) then
                sync_next_state <= CNV_LP;
                if rising_edge(clk) then
                    BUSY_cntrl_cnt <= std_logic_vector(unsigned(BUSY_cntrl_cnt) + 1);
                end if;
            else
                sync_next_state <= waitBUSY;
            end if;

        when waitBUSY =>
            if (unsigned(BUSY_cntrl_cnt) < 30) then
                sync_next_state <= waitBUSY;
                if rising_edge(clk) then
                    BUSY_cntrl_cnt <= std_logic_vector(unsigned(BUSY_cntrl_cnt) + 1);
                end if;
            else
                sync_next_state <= waitSYNC;
                if rising_edge(clk) then
                    BUSY_cntrl_cnt <= (others => '0');
                end if;
            end if;
    end case ;

end process;

BUSY <= '1' when sync_state = waitBUSY else '0';

--!--------------------------CONV_X FSM--------------------------!--
process(clk, rst) 
 begin
    if rising_edge(clk) then 
        if rst = '1' then
            cnv_state <= cnv_idle;
        else
            cnv_state <= cnv_next_state;
        end if ;

    end if;
end process;

process(clk, cnv_state, SYNC, rst) 
 begin
    if rst = '1' then
        cnvs_cntrl_cnt <= (others => '0');
    end if;

    case( cnv_state ) is
    
        when cnv_idle =>
            if SYNC = '1' and SPI_READY = '1' and BUSY = '0' then                                  --! and SPI_READY = '1'
                cnv_next_state <= conv;
            else 
                cnv_next_state <= cnv_idle;
            end if;
                
        when conv => 

            if (unsigned(cnvs_cntrl_cnt) < CNVS_CLK_LP-1) then
                cnv_next_state <= conv;
                if rising_edge(clk) then
                    cnvs_cntrl_cnt <= std_logic_vector(unsigned(cnvs_cntrl_cnt) + 1);
                end if;
            else
                cnv_next_state <= cnv_idle;
                if rising_edge(clk) then
                    cnvs_cntrl_cnt <= (others => '0');
                end if;
            end if;

        when others =>
                cnv_next_state <= cnv_idle;
    end case ;
 end process;

CONVST_A <= '0' when cnv_state = conv else '1';
CONVST_B <= '0' when cnv_state = conv else '1';
CONVST_X <= '0' when cnv_state = conv else '1';
RESET    <= rst;


--!--------------------------SPI FSM-----------------------------!--
process(clk) 
 begin
    if rising_edge(clk) then 
        if rst = '1' then
            fsm_spi_curr_state <= spi_reset;
        else
            fsm_spi_curr_state <= fsm_spi_next_state;
        end if ;

    end if;
end process;

process(fsm_spi_curr_state, BUSY, clk, rst)
 begin

    case( fsm_spi_curr_state ) is

        when spi_reset => 
        
            SPI_READY <= '1';
            spi_transmit_cnt <= (others => '0');

            if BUSY = '1' then
                fsm_spi_next_state <= idle;
            else 
                fsm_spi_next_state <= spi_reset;
            end if ;

        when idle =>
            if (BUSY = '0') then 
                fsm_spi_next_state <= transmit;
                if rising_edge(clk) then
                    SPI_READY <= '0';
                end if;
            else
                fsm_spi_next_state <= idle;
            end if;

        when transmit =>
            if (BUSY = '0' and unsigned(spi_transmit_cnt) < (SPI_TRANMIT_DELAY*(CLK_DIV+1)*2) - 1 ) then      --?and unsigned(spi_transmit_cnt) < SPI_TRANMIT_DELAY I (заканчивать транзакцию при передачи нужного числа бит)  
                fsm_spi_next_state <= transmit;                                                               --?                                                     (даже если BUSY еще 0(можно продолжать передачу)      )       
                if rising_edge(clk) then
                    spi_transmit_cnt <= std_logic_vector(unsigned(spi_transmit_cnt) + 1);
                end if;
            else
                fsm_spi_next_state <= spi_reset;                                                              --? fsm_spi_next_state <= spi_reset;                 II

                if rising_edge(clk) then
                    spi_transmit_cnt <= (others => '0');
                    SPI_READY <= '1';
                end if;
            end if ;

        when others => 
                fsm_spi_next_state  <= spi_reset;

    end case ;
end process;

CS <= '0' when fsm_spi_curr_state = transmit else '1';  

--!--------------------------CLK devider-------------------------!--
sclk_PROC : process(clk, rst)
 begin
    if rising_edge(clk) then
        if rst = '1' then
            sclk_devider_cnt <= "00010";                                            --! reset counter must be accomodate with the init value
        end if ;                                              
    end if ;

    if SPI_READY = '0' then 
        if rising_edge(clk) then   
            if unsigned(sclk_devider_cnt) = CLK_DIV then
                sclk_devider_cnt <= (others => '0');
                sclk <= not sclk;
            else
                sclk_devider_cnt <= std_logic_vector(unsigned(sclk_devider_cnt) + 1);
            end if ;
        end if;
    else
        sclk <= '1';
        sclk_devider_cnt <= "00010";                                                --! reset counter must be accomodate with the init value
    end if;

end process;

--!--------------------------SRGn--------------------------------!--
process(sclk)
 begin

    if falling_edge(sclk) then
        if rst = '1' then
            SRGn <= (others => '0');
        else
            SRGn <= SRGn(16 downto 0) & SDATA;
        end if;
    end if ;
end process;
ODATA <= SRGn;


end conv_cntrl_behavioral ; -- conv_cntrl_behavioral


--ToDo закинуть spi в машину состояний cnv
--ToDo перепроверить reset и игнорирование левых SYNC
--ToDo сделать делитель частоты, добавить sclk