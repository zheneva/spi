
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_master_test is
end spi_master_test;


architecture spi_master_test_struct of spi_master_test is

  component spi_master
    port (
      clk : in std_logic;
      rst : in std_logic
    );

  end component;

  signal clk : std_logic := '0';
  signal rst : std_logic;

begin

circ_spi_master: spi_master
              port map( clk => clk, rst => rst );

process
 begin
  wait for 20 ns;
  clk <= not clk;
end process;


end spi_master_test_struct ; -- spi_master_test_struct