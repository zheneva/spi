
library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_master is
  port (
    clk : in std_logic;
    rst : in std_logic
  );
end spi_master;


architecture spi_master_behavioral of spi_master is

  signal counter : std_logic_vector(3 downto 0) := (others => '0');
  signal CS : std_logic := '1' ;

  signal s_clk : std_logic := '1' ;

begin

process(clk)
begin
 if rising_edge(clk) then 
    if (counter = "0000") then
      CS <= '1';
    else
      CS <= '0';
    end if;
end if;
end process;

process(clk, CS)
 begin
    if (CS = '0') then
      s_clk <= clk;
    else
      s_clk <= '1';
    end if;

end process;

process(clk)
 begin
 if rising_edge(clk) then 
  counter <= std_logic_vector(unsigned(counter) + 1);
 end if;
end process;

end spi_master_behavioral ; -- spi_master_behavioral