
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name adc_cntr_prjctl -dir "D:/Androsov/spi/ADC_control/ise/adc_cntr_prjctl/planAhead_run_2" -part xc6slx9tqg144-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Androsov/spi/ADC_control/ise/adc_cntr_prjctl/adc_cntrl_tl_cs.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Androsov/spi/ADC_control/ise/adc_cntr_prjctl} {ipcore_dir} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "adc_cntrl_tl.ucf" [current_fileset -constrset]
add_files [list {adc_cntrl_tl.ucf}] -fileset [get_property constrset [current_run]]
link_design
