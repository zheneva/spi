// CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
// Example: 25 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 217


//  TX_DV     ______________|___________________________________________________________________________________
//                          \                     
//  State     <     RST      ><                                 RX                                   ><   RST  >  
//                           \
//  wait_cnt  <  CLKs_per_BIT ><0><1><2>.....< CLKs_per_BIT > <0><1><2>.....< CLKs_per_BIT >
// en_RX_cnt  ________________|______________________________|______________________________|..._|________________  
//                             \                              \                              \         
// RX_cnt     <        0       > <              1             ><              2              >...< A ><    0     >


module uart_transmitter 
#(
    parameter  CLKs_per_BIT = 217
)
(
    input  clk,
    input  i_TX_DV,
    input [7:0] i_TX_byte,
    output reg o_TX_serial
);
    

   
    localparam RST  = 1'b0;
    localparam RX   = 1'b1;

    reg State, nextState;

    reg [10:0] wait_cnt = CLKs_per_BIT;
    reg [ 3:0]   RX_cnt = 4'd0;

    reg      en_RX_cnt = 1'b0;   
    reg         TX_DV  = 1'b0;

    reg [ 7:0] TX_byte ; 



    always @(posedge clk) begin
        TX_DV   <= i_TX_DV;

        if (TX_DV == 1) begin
            TX_byte <= i_TX_byte;
        end

    end

    always @(posedge clk) begin
        State <= nextState;
    end


    always @(*) begin
        case (State)

            RST : begin
                if (TX_DV == 1'b1)   
                    nextState <= RX;
                else
                    nextState <= RST;
            end

            RX  : begin
                if ((RX_cnt == 4'd10) && (en_RX_cnt == 1'b1)) 
                    nextState <= RST;
                else     
                    nextState <= RX;
            end

            default: nextState <= RST;

        endcase
    end

    always @( posedge clk) begin

        if (State == RST) begin
            wait_cnt  <= CLKs_per_BIT-1;	         //! 	 wait_cnt  <= CLKs_per_BIT leeds to no delay between RX_DV bit appers and data to be sampled
            RX_cnt    <= 4'd0;
            en_RX_cnt <= 1'b0;		
        end  

        if (State == RX) begin
            if (wait_cnt == CLKs_per_BIT-1) begin
                wait_cnt  <= 10'd0;
                en_RX_cnt <= 1'b1;
            end else begin
                wait_cnt  <= wait_cnt + 10'd1;
                en_RX_cnt <= 1'b0;
            end
        end


        if (en_RX_cnt == 1'b1) begin
            if (RX_cnt == 10) 
                RX_cnt <= 4'd0;
            else 
                RX_cnt <= RX_cnt + 4'd1; 
            
        end
    end


    always @(*) begin
        //if (en_RX_cnt == 1) begin
            case (RX_cnt)
                'd0:  o_TX_serial = 1'b1;
                'd1:  o_TX_serial = 1'b0;
                'd2:  o_TX_serial = TX_byte[0];
                'd3:  o_TX_serial = TX_byte[1];
                'd4:  o_TX_serial = TX_byte[2];
                'd5:  o_TX_serial = TX_byte[3];
                'd6:  o_TX_serial = TX_byte[4];
                'd7:  o_TX_serial = TX_byte[5];
                'd8:  o_TX_serial = TX_byte[6];
                'd9:  o_TX_serial = TX_byte[7];
                'd10: o_TX_serial = 1'b1;
                default: o_TX_serial = 1'b1;
            endcase
        //end 
    end

endmodule

//ToDo загнать i_TX_byte в регистр RX_byte и устанавливать его только по сигналу i_TX_DV 