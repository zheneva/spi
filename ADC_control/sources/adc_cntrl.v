

// (* DONT_TOUCH = "yes" *)

module adc_cntrl #(
        parameter DIV_factor        = 3
    ,   parameter SPI_CNT           = 72
    ,   parameter CONVSTx_lowPulse  = 3
    ,   parameter CONVSTx_cycle     = 3000  //600   //900910
) (
        input             clk
    ,   input             i_SCLK
    ,   input             i_reset
    ,   input             i_BUSY
    //,   input             i_FRSTDATA
    ,   input             i_DOUTA
    ,   input             i_DOUTB
    ,   output            o_CS
    ,   output            CONVSTx
    // ,   output     [17:0] o_DataA_1
    // ,   output     [17:0] o_DataA_2
    // ,   output     [17:0] o_DataA_3
    // ,   output     [17:0] o_DataA_4
    // ,   output     [17:0] o_DataB_1
    // ,   output     [17:0] o_DataB_2
    // ,   output     [17:0] o_DataB_3
    // ,   output     [17:0] o_DataB_4
    ,   output        [71:0] o_DOUTA
    ,   output        [71:0] o_DOUTB
    ,   output            o_reset    
);
    
    reg busy_0_pipe, busy_1_pipe; 
    reg reset_0_pipe, reset_1_pipe; 
    //reg FRSTDATA_0_pipe, FRSTDATA_1_pipe;



    reg [15:0] spi_transn_cnt = 15'd0;
    reg        en_spi_transn_cnt = 1'b0;




//---------wake-up control--------------//

    reg [15:0] wake_cnt = 16'd0;
    reg       wake_rst_0, wake_rst_1;

    always @(posedge clk) begin
        if (wake_cnt == 16'd5000) begin
            wake_cnt   <= wake_cnt;
            wake_rst_0 <= 1'b0;
        end else begin
            wake_cnt   <= wake_cnt + 16'd1;
            wake_rst_0 <=  1'b1;
        end
            wake_rst_1 <= wake_rst_0;
    end





//---------reset--------------//

    reg [7:0] reset_cnt = 8'd6;

    always @(posedge clk) begin 
            reset_0_pipe    <= i_reset   ; reset_1_pipe    <= reset_0_pipe;
    end

    always @(posedge clk) begin
        if ((reset_1_pipe == 1'b1 && reset_0_pipe == 1'b0) || (wake_rst_1 == 1'b1 && wake_rst_0 == 1'b0 )) begin
            reset_cnt <= 8'd0;
        end else begin
            if (reset_cnt == 8'd6) 
                reset_cnt <= 8'd6;
            else
                reset_cnt <= reset_cnt + 8'd1;
        end
    end
    
    
    wire s_reset;
    assign o_reset = ( reset_cnt !== 6) ? 1'b1 : 1'b0; 
    assign s_reset = ( reset_cnt !== 6 || reset_1_pipe == 1'b1) ? 1'b1 : 1'b0; 

    //assign o_wake_rst = wake_rst_0;  
    //assign s_reset = reset_1_pipe; 


//---------pipe_async_signals------------//
    // always @(posedge clk) begin 
    //     if (s_reset == 1) begin
    //         FRSTDATA_0_pipe <= 0; FRSTDATA_1_pipe <= 0;
    //     end else begin
    //         FRSTDATA_0_pipe <= i_FRSTDATA; FRSTDATA_1_pipe <= FRSTDATA_0_pipe;
    //     end
    // end



//--------------CONVSTx------------------//

    reg [14:0] conversion_cnt = 15'd0;
    
    always @(posedge clk) begin
        if (s_reset == 1) begin 
            conversion_cnt <= 15'd0;
        end else begin 
            if (conversion_cnt == CONVSTx_cycle ) 
                    conversion_cnt <= 15'd0;
                else    
                    conversion_cnt <= conversion_cnt + 15'd1;
        end
    end

    assign CONVSTx = (conversion_cnt > 0 && conversion_cnt < CONVSTx_lowPulse) ? 1'b0 : 1'b1;





//--------------receive BUSY | form CS---------------------//
    always @(posedge i_SCLK) begin 
        if (s_reset == 1) begin
            busy_0_pipe     <= 0; busy_1_pipe     <= 0;
        end else begin
            busy_0_pipe     <= i_BUSY    ; busy_1_pipe     <= busy_0_pipe;
        end
    end

    reg r_CS = 1'b1;

    always @(posedge i_SCLK) begin
        if (busy_0_pipe == 1'b0 && busy_1_pipe == 1'b1) begin
            r_CS <= 1'b0;
        end 
		  
        if (s_reset == 1) begin
            spi_transn_cnt <= 15'd0;
				r_CS 				<= 1'b1 ;
        end else begin
            if (r_CS == 1'b0) begin
                if (spi_transn_cnt == SPI_CNT-1) begin 
                    spi_transn_cnt    <= 15'd0;
                    r_CS <= 1'b1;
                end else 
                    spi_transn_cnt <= spi_transn_cnt + 15'd1;
            end
        end    
    end

    assign o_CS = r_CS;



//--------------REGs------------------//
    reg [71:0] r_DOUTA = 72'd0;
    reg [71:0] r_DOUTB = 72'd0;
	
    wire n_i_SCLK;

    assign n_i_SCLK = ~i_SCLK;

    always @(posedge n_i_SCLK )
    begin
        if (o_CS == 1'b0) begin
            r_DOUTA[71:0] <= {r_DOUTA[70:0], i_DOUTA};
            r_DOUTB[71:0] <= {r_DOUTB[70:0], i_DOUTB};
        end
    end


    assign o_DOUTA[71:0] = o_CS == 1'b1 ? r_DOUTA[71:0] : 71'd0;
    assign o_DOUTB[71:0] = o_CS == 1'b1 ? r_DOUTB[71:0] : 71'd0;


// (* dont_touch = "yes" *) wire [17:0] o_DataA_1;
// (* dont_touch = "yes" *) wire [17:0] o_DataA_2;
// (* dont_touch = "yes" *) wire [17:0] o_DataA_3;
// (* dont_touch = "yes" *) wire [17:0] o_DataA_4;
// (* dont_touch = "yes" *) wire [17:0] o_DataB_1;
// (* dont_touch = "yes" *) wire [17:0] o_DataB_2;
// (* dont_touch = "yes" *) wire [17:0] o_DataB_3;
// (* dont_touch = "yes" *) wire [17:0] o_DataB_4;

// assign o_DataA_1[17:0] = o_CS == 1'b1 ? r_DOUTA[71:54] : 18'd0;
// assign o_DataA_2[17:0] = o_CS == 1'b1 ? r_DOUTA[53:36] : 18'd0;
// assign o_DataA_3[17:0] = o_CS == 1'b1 ? r_DOUTA[35:18] : 18'd0;
// assign o_DataA_4[17:0] = o_CS == 1'b1 ? r_DOUTA[17:0]  : 18'd0;
// assign o_DataB_1[17:0] = o_CS == 1'b1 ? r_DOUTB[71:54] : 18'd0;
// assign o_DataB_2[17:0] = o_CS == 1'b1 ? r_DOUTB[53:36] : 18'd0;
// assign o_DataB_3[17:0] = o_CS == 1'b1 ? r_DOUTB[35:18] : 18'd0;
// assign o_DataB_4[17:0] = o_CS == 1'b1 ? r_DOUTB[17:0]  : 18'd0;


endmodule


//ToDo делить частоты 
//ToDo регистры длля хранения данных
