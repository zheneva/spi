`timescale 1 ns / 1 ns

module adc_cntrl_tb #(
        parameter DIV_factor        = 5
    ,   parameter SPI_CNT           = DIV_factor*18
    ,   parameter CONVSTx_lowPulse  = 3
    ,   parameter CONVSTx_cycle     = 600
)(

);
    

reg  clk;
reg  i_SCLK;
reg  i_reset;
reg  i_BUSY;
wire i_FRSTDATA;
wire i_DOUTA;
wire i_DOUTB;
wire [17:0] o_DOUTA;
wire [17:0] o_DOUTB;
wire o_CS;
wire o_SCLK;
wire CONVSTx;

    initial begin
        clk = 1'b0; i_reset <= 1'b0; i_SCLK <= 0;
    end

    always #10 clk <= ~clk;

    always #50 i_SCLK <= ~i_SCLK;


    always @(posedge CONVSTx) begin
        #0  ; i_BUSY <= 1'b1;
        #19000; i_BUSY <= 1'b0;    
    end

reg [17:0] t_DOUTA = 18'b111111000000111111;

    always @(posedge o_SCLK) begin
        if (o_CS == 1'b0) begin
            t_DOUTA[17:0] <= {t_DOUTA[16:0],t_DOUTA[17]};            
        end
    end

    assign i_DOUTA = t_DOUTA;

adc_cntrl  #(.DIV_factor(DIV_factor), .SPI_CNT(SPI_CNT))  adc_c
(
        .clk(clk)
    ,   .i_SCLK(i_SCLK)
    ,   .i_reset(i_reset)
    ,   .i_BUSY(i_BUSY)
    ,   .i_FRSTDATA(i_FRSTDATA)
    ,   .i_DOUTA(i_DOUTA)
    ,   .i_DOUTB(i_DOUTB)
    ,   .o_CS( o_CS)
    ,   .o_SCLK( o_SCLK)
    ,   .CONVSTx( CONVSTx)
    ,   .o_DOUTA(o_DOUTA)
    ,   .o_DOUTB(o_DOUTB)
);


endmodule