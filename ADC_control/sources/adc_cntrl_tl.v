`timescale 1ns / 1ps


// CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
// Example: 25 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 217
// Example: 50 MHz Clock, 256000 baud UART
// (50000000)/(256000) = 195

(* DONT_TOUCH = "yes" *)

module adc_cntrl_tl #(
        parameter DIV_factor        = 5
    ,   parameter SPI_CNT           = 72
    ,   parameter CONVSTx_lowPulse  = 3*5
    ,   parameter CONVSTx_cycle     = 18000
    ,   parameter CLKs_per_BIT      = 195
)
(
        input             clk
    ,   input             i_reset
    ,   input             i_BUSY
//    ,   input             i_FRSTDATA
    ,   input             i_DOUTA
    ,   input             i_DOUTB
    ,   input      [ 2:0] sw
//    ,   input             i_SCLK
    ,   output            o_CS
    ,   output            o_SCLK
    ,   output            CONVSTx
    // ,   output     [17:0] o_DataA_1
    // ,   output     [17:0] o_DataA_2
    // ,   output     [17:0] o_DataA_3
    // ,   output     [17:0] o_DataA_4
    // ,   output     [17:0] o_DataB_1
    // ,   output     [17:0] o_DataB_2
    // ,   output     [17:0] o_DataB_3
    // ,   output     [17:0] o_DataB_4
    // ,   output     [17:0] o_DOUTA
    // ,   output     [17:0] o_DOUTB
    ,   output            no_bip
    ,   output            o_reset
    ,   output            o_TX_serial
    ,   output     [11:0] LEDR
    ,   output     [ 7:0] SIND
);







//__________PLL_clok_gen__________//
    clk_wiz pll_clk(
        .sysCLK_50MHz(clk),      
        .CLK_50Mhz(CLK_50Mhz),    
        .CLK_10MHz(CLK_10MHz)
    );


//___________adc_control__________//

    wire [71:0] o_DOUTA;
    wire [71:0] o_DOUTB;

    adc_cntrl  #(.DIV_factor(DIV_factor), .SPI_CNT(SPI_CNT), .CONVSTx_lowPulse(CONVSTx_lowPulse), .CONVSTx_cycle(CONVSTx_cycle))  adc_c
    (
            .clk(CLK_50Mhz)
        ,   .i_SCLK(CLK_10MHz)    
        ,   .i_reset(i_reset)
        ,   .i_BUSY(i_BUSY)
        ,   .i_DOUTA(i_DOUTA)
        ,   .i_DOUTB(i_DOUTB)
        ,   .o_CS( o_CS)
        ,   .CONVSTx( CONVSTx)
        ,   .o_DOUTA(o_DOUTA) 
        ,   .o_DOUTB(o_DOUTB) 
        ,   .o_reset(o_reset)
    );


//___________SWICH_CHANELS_____________//

    reg [17:0] o_DOUTx;

    always @(*) begin
        case (sw)
            3'b000: o_DOUTx[17:0] = o_DOUTA[71:54]; // 18'd0 ;// o_DOUTA[71:54]; //o_DOUTA[71:54]; // o_DOUTA[17:0 ];
            3'b001: o_DOUTx[17:0] = o_DOUTA[53:36]; // 18'd1 ;// o_DOUTA[53:36]; // o_DOUTA[35:18];
            3'b010: o_DOUTx[17:0] = o_DOUTA[35:18]; // 18'd2 ;// o_DOUTA[35:18]; // o_DOUTA[53:36];
            3'b011: o_DOUTx[17:0] = o_DOUTA[17:0 ]; // 18'd3 ;// o_DOUTA[17:0 ]; // o_DOUTA[71:54];
            3'b100: o_DOUTx[17:0] = o_DOUTB[71:54]; // 18'd4 ;// o_DOUTB[71:54]; // o_DOUTB[17:0 ];
            3'b101: o_DOUTx[17:0] = o_DOUTB[53:36]; // 18'd5 ;// o_DOUTB[53:36]; // o_DOUTB[35:18];
            3'b110: o_DOUTx[17:0] = o_DOUTB[35:18]; // 18'd6 ;// o_DOUTB[35:18]; // o_DOUTB[53:36];
            3'b111: o_DOUTx[17:0] = o_DOUTB[17:0 ]; // 18'd7 ;// o_DOUTB[17:0 ]; // o_DOUTB[71:54];
            default: 
				    o_DOUTx[17:0] = o_DOUTA[71:54]; // o_DOUTB[35:18]; //o_DOUTA[71:54];
        endcase
    end


//___________UART transmitter___________//

    // CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
    // Example: 25 MHz Clock, 115200 baud UART
    // (25000000)/(115200) = 217


    reg CS_pipe_0, CS_pipe_1;         

    always @(posedge CLK_50Mhz) begin
        CS_pipe_0 <= o_CS; CS_pipe_1 <= CS_pipe_0;   // need to catch o_CS rising edge => means all chenels were sampled in registers => data valid
    end

    wire      i_TX_DV ;
    reg  [7:0]i_TX_byte;

    reg     en_byte_cnt   = 1'b0;
    reg     en_wait_cnt   = 1'b0;
    reg [ 2:0] byte_cnt   = 4'd0;
    reg [11:0] wait_cnt   = 12'd0;
    reg [ 7:0] tran_cnt   = 8'd0;         // sends the namber of captured data (номер посылки)

    always @(posedge CLK_50Mhz) begin

        if (i_reset == 1'b1) begin
            wait_cnt    <= CLKs_per_BIT-1;	  
            byte_cnt    <= 4'd0;
            en_byte_cnt <= 1'b0;		
            en_wait_cnt <= 1'b0;
            tran_cnt    <= 8'd0;	
        end  

        if ((CS_pipe_0 == 1'b1) && (CS_pipe_1 == 1'b0)) begin
            en_wait_cnt <= 1'b1;
            tran_cnt <= tran_cnt + 8'd1;
        end

        if (en_wait_cnt == 1'b1) begin
            if (wait_cnt == (CLKs_per_BIT)*11+1) begin //! должен быть другой похожу 195 * 11 bit   
                wait_cnt    <= 12'd0;
                en_byte_cnt <= 1'b1;
            end else begin
                wait_cnt    <= wait_cnt + 12'd1;
                en_byte_cnt <= 1'b0;
            end
        end


        if (en_byte_cnt == 1'b1) begin
            if (byte_cnt == 3'd6) begin
                byte_cnt    <= 3'd0;
                en_wait_cnt <= 1'b0;
            end else 
                byte_cnt <= byte_cnt + 3'd1;
        end
    end  


    always @(*) begin

        case (byte_cnt)
            4'd0 : i_TX_byte = 8'h55;
            4'd1 : i_TX_byte = 8'hAA;
            4'd2 : i_TX_byte = 8'h03;                   // data size 
            4'd3 : i_TX_byte = tran_cnt[7:0];           // data size 8'b11001100; //
            4'd4 : i_TX_byte = o_DOUTx[ 7:0];           //o_DOUTA[ 7:0];           //! o_DOUTx[ 7:0];
            4'd5 : i_TX_byte = o_DOUTx[15:8];           //o_DOUTA[15:8];           //! o_DOUTx[15:8];
            4'd6 : i_TX_byte = {{6'b000000},o_DOUTx[17:16]};
            default: i_TX_byte = 8'b00000000;
        endcase

    end            

    assign i_TX_DV   =  en_byte_cnt;  

    uart_transmitter #( .CLKs_per_BIT(CLKs_per_BIT)) UR
    (
            .clk(CLK_50Mhz)
        ,   .i_TX_DV(i_TX_DV)
        ,   .i_TX_byte(i_TX_byte)  //i_TX_byte
        ,   .o_TX_serial(o_TX_serial)
    );


//__________clock forwarding____________//
    ODDR2  #(
            .DDR_ALIGNMENT("NONE")  // Sets output alignment to "NONE", "C0" or "C1"
    ,    .INIT(1'b0)                // Sets initial state of the Q output to 1'b0 or 1'b1
    ,    .SRTYPE("SYNC")            // Specifies "SYNC" or "ASYNC" set/reset
    ) my_oddr2 (

            .D0(1'b1)
        ,	.D1(1'b0)
        ,	.C0(CLK_10MHz)
        ,	.C1(~CLK_10MHz)
        ,	.R(1'b0)
        ,	.S(1'b0)
        ,	.CE(1'b1)
        ,	.Q(o_SCLK)
        
    );


//__________default_assignments_____________//;

    assign no_bip =  1'b1;
    assign LEDR   = 12'b111_111_111_111;
    assign SIND   =  8'b1111_1111;
//	 assign o_SCLK = o_SCLK;

endmodule
