// CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
// Example: 25 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 217


// 40 ns clk period => 25MHz

module uart_transmitter_test #(parameter DELAY = 20)
(
);
    

reg clk;
reg i_TX_DV;
reg [7:0] i_TX_byte;
wire o_TX_serial;
integer i;


initial begin
    clk = 1'b0;
    i_TX_DV = 1'b0;
    i_TX_byte = 0;

    for (i = 0; i < 20 ; i = i + 1) begin
            #((DELAY*40*217)-40)                            //! *217 due to 25MHz clk and 115200 data rate => new data bit appears every 217 clk cycles * 40 ns 
                i_TX_byte = $random;
                i_TX_DV = 1'b1;
            #(40)
                i_TX_DV = 1'b0;
    end


end


always  #20 clk = ~clk;



uart_transmitter #( .CLKs_per_BIT(217),  .serial_bits_number(8) ) UR
(
        .clk(clk)
    ,   .i_TX_DV(i_TX_DV)
    ,   .i_TX_byte(i_TX_byte)
    ,   .o_TX_serial(o_TX_serial)
);
endmodule

